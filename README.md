The Courses
==============================


Here's the new "Learn Spring" course: <br/>
**[>> LEARN SPRING - THE MASTER CLASS](https://www.baeldung.com/learn-spring-course?utm_source=github&utm_medium=social&utm_content=tutorials&utm_campaign=ls#master-class)**

Here's the Master Class of "REST With Spring" (along with the new announced Boot 2 material): <br/>
**[>> THE REST WITH SPRING - MASTER CLASS](https://www.baeldung.com/rest-with-spring-course?utm_source=github&utm_medium=social&utm_content=tutorials&utm_campaign=rws#master-class)**

And here's the Master Class of "Learn Spring Security": <br/>
**[>> LEARN SPRING SECURITY - MASTER CLASS](https://www.baeldung.com/learn-spring-security-course?utm_source=github&utm_medium=social&utm_content=tutorials&utm_campaign=lss#master-class)**



Java and Spring Tutorials
================

This project is **a collection of small and focused tutorials** - each covering a single and well defined area of development in the Java ecosystem. 
A strong focus of these is, of course, the Spring Framework - Spring, Spring Boot and Spring Security. 
In additional to Spring, the modules here are covering a number of aspects in Java. 

Profile based segregation
====================

We are using maven build profiles to segregate the huge list of individual projects we have in our repository.

The projects are broadly divided into 3 list: first, second and heavy. 

Next, they are segregated further on the basis of tests that we want to execute.

Therefore, we have a total of 6 profiles:

| Profile                 | Includes                    | Type of test enabled |
| ----------------------- | --------------------------- | -------------------- |
| default-first           | First set of projects       | *UnitTest            |
| integration-lite-first  | First set of projects       | *IntegrationTest     |
| default-second          | Second set of projects      | *UnitTest            |
| integration-lite-second | Second set of projects      | *IntegrationTest     |
| default-heavy           | Heavy/long running projects | *UnitTest            |
| integration-heavy       | Heavy/long running projects | *IntegrationTest     |

Building the project
====================

Though it should not be needed often to build the entire repository at once because we are usually concerned with a specific module.

But if we want to, we can invoke the below command from the root of the repository if we want to build the entire repository with only Unit Tests enabled:

`mvn clean install -Pdefault-first,default-second,default-heavy`

or if we want to build the entire repository with Integration Tests enabled, we can do:

`mvn clean install -Pintegration-lite-first,integration-lite-second,integration-heavy`


Building a single module
====================
To build a specific module run the command: `mvn clean install` in the module directory


Running a Spring Boot module
====================
To run a Spring Boot module run the command: `mvn spring-boot:run` in the module directory


Working with the IDE
====================
This repo contains a large number of modules. 
When you're working with an individual module, there's no need to import all of them (or build all of them) - you can simply import that particular module in either Eclipse or IntelliJ. 


Running Tests
=============
The command `mvn clean install` from within a module will run the unit tests in that module.
For Spring modules this will also run the `SpringContextTest` if present.

To run the integration tests, use the command:

`mvn clean install -Pintegration-lite-first` or 

`mvn clean install -Pintegration-lite-second` or 

`mvn clean install -Pintegration-heavy`

depending on the list where our module exists




## weworkapi_java-master
企业微信 加解密方法
https://open.dingtalk.com/document/resourcedownload/download-server-sdk
```java
import java.security.MessageDigest;

static void main(String[] args)
{
    String keyStr = "key";
    SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmm"); //设置日期格式
    List < Map.Entry < String, Object >> list;
    Map < String, Object > pairs = new HashMap();
    pairs.put("time", df.format(new Date()));
    pairs.put("account", "tianmin");
    list = new ArrayList < Map.Entry < String, Object >> (pairs.entrySet());
    Collections.sort(list, new Comparator < Map.Entry < String, Object >> ()
    {
        @Override
        public int compare(Map.Entry < String, Object > firstMapEntry, Map.Entry < String, Object > secondMapEntry)
        {
            return firstMapEntry.getKey().compareTo(secondMapEntry.getKey());
        }
    });
    String sign = "";
    for(Map.Entry < String, Object > item: list)
    {
        sign += item.getKey() + "=" + item.getValue() + "&";
    }
    sign += keyStr;
    System.out.println(sign);
    System.out.println(stringToMD5(sign));
}
public static String stringToMD5(String plainText)
{
    byte[] secretBytes = null;
    try
    {
        secretBytes = MessageDigest.getInstance("md5").digest(plainText.getBytes());
    }
    catch(NoSuchAlgorithmException e)
    {
        throw new RuntimeException("没有这个md5算法！");
    }
    String md5code = new BigInteger(1, secretBytes).toString(16);
    for(int i = 0; i < 32 - md5code.length(); i++)
    {
        md5code = "0" + md5code;
    }
    return md5code;
}



```

## 字符串拼接
https://www.bilibili.com/video/BV1zL411P7e1/?spm_id_from=pageDriver&vd_source=571fe4d0abe769e5a5a5fa69e4a8a0c4

## 原码 反码 补码
正数, 原码 反码 补码一致

原码  数据二进制状态

反码  符号位不变 ,其余各位取反

补码  反码+1,就是补码

## Runtime
Runtime.getRuntime().availabeProcessors()
Runtime.freeMemory()
Runtime.maxMemory()
Runtime.getRuntime().exec("calc");

## Vector  Arrarylist 区别
Vector 大多数方法是线程安全的,主要在多线程中,每次扩容是2倍

数组查询速度快,链表插入快

## set
### hash表
#### hashset 无序
底层是用hashmap
一个数值的高低位异或运算,散列更强
#### Hashmap

### 二叉树[左节点小于右节点]
treeset 是基于TreeMap, 底层是用二叉树实现,有序

## 对象排序
要实现 Comparable接口
return  1 大到小
return -1 小到大

# try-ressource jdk-7
try(){}catch(exception ee){}
# 配置文件  properties xml json
properties  编码是 iso-8859-1 ,不支持中文
java.util.properties
```java
import java.util.*;
//https://hollischuang.gitee.io/tobetopjavaer/#/basics/java-basic/fail-fast-vs-fail-safe
 List<String> userNames = new CopyOnWriteArrayList<String>() {{
        add("Hollis");
        add("hollis");
        add("HollisChuang");
        add("H");
    }};

    userNames.iterator();

    for (String userName : userNames) {
        if (userName.equals("Hollis")) {
            userNames.remove(userName);
        }
    }
```

# java多线程
## 继承 Thread
```java
class myThread extends Thread{
@ovveride
public void run(){}
}

Thread.currentThread() 当前线程
// 创建线程
Callable Future 接口

锁
synchronized 重量 向操作系统申请锁太大了,大重了
lock  轻量
```


## fastjson
https://github.com/alibaba/fastjson2
```java
String text = "{\"id\": 2,\"name\": \"fastjson2\"}";
JSONObject obj = JSON.parseObject(text);

int id = obj.getIntValue("id");
String name = obj.getString("name");

String text = "[2, \"fastjson2\"]";
JSONArray array = JSON.parseArray(text);

int id = array.getIntValue(0);
String name = array.getString(1);
```
将JSON解析为Java对象
```java
String text = "...";
User data = JSON.parseObject(text, User.class);
```
//第三种方式

Map mapType = JSON.parseObject(str,Map.class);

System.out.println("这个是用JSON类,指定解析类型，来解析JSON字符串!!!");

for (Object obj : mapType.keySet()){

System.out.println("key为："+obj+"值为："+mapType.get(obj));

}
原文链接：https://blog.csdn.net/weixin_29218509/article/details/111949345