1.简单粗暴：https://www.jianshu.com/p/708604084baa
private void uploadFile(String url,File file) {
        OkHttpClient okHttpClient = new OkHttpClient();
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("platform","android")
                .addFormDataPart(
                        "file",
                        file.getName(),
                        RequestBody.create(MediaType.parse(guessMimeType(file.getAbsolutePath())), file)
                );
        Request request = new Request.Builder()
                .url(url)
                .post(builder.build())
                .build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("TAG","请求失败");
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.e("TAG","结果=="+response.body().string());
            }
        });
    }

    private String guessMimeType(String absolutePath) {
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String contentTypeFor = fileNameMap.getContentTypeFor(absolutePath);
        if(TextUtils.isEmpty(contentTypeFor)){
            return "application/octet-stream";
        }
        return contentTypeFor;
    }
2.带进度条下载
package com.miaozi.myokhttp;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.ForwardingSink;
import okio.Okio;
/**
* created by panshimu
* on 2019/11/11
*/
public class ProgressMultipartBody extends RequestBody {
   private RequestBody mRequestBody;
   private long mCurrentLength;
   private UploadProgressListener mListener;
   public ProgressMultipartBody(RequestBody requestBody) {
       this.mRequestBody = requestBody;
   }
   public ProgressMultipartBody(RequestBody requestBody,UploadProgressListener progressListener) {
       this.mRequestBody = requestBody;
       this.mListener = progressListener;
   }
   @Override
   public MediaType contentType() {
       return mRequestBody.contentType();
   }

   @Override
   public long contentLength() throws IOException {
       return mRequestBody.contentLength();
   }

   @Override
   public void writeTo(BufferedSink sink) throws IOException {
       ForwardingSink forwardingSink = new ForwardingSink(sink) {
           @Override
           public void write(Buffer source, long byteCount) throws IOException {
               mCurrentLength += byteCount;
               if(mListener != null)
                   mListener.onProgress(contentLength(),mCurrentLength);
               super.write(source, byteCount);
           }
       };
       BufferedSink bufferedSink = Okio.buffer(forwardingSink);
       mRequestBody.writeTo(bufferedSink);
       bufferedSink.flush();
   }
}

package com.miaozi.myokhttp;

/**
 * created by panshimu
 * on 2019/11/11
 */
public interface UploadProgressListener {
    void onProgress(long total,long progress);
}

调用方式：

ProgressMultipartBody progressMultipartBody = new ProgressMultipartBody(builder.build(), new UploadProgressListener() {
            @Override
            public void onProgress(long total, long progress) {
                Log.e("TAG","当前下载进度：" + progress);
            }
        });

        Request request = new Request.Builder()
                .url(url)
                .post(progressMultipartBody)
                .build();
3.文件下载
 private void downLoadFile() {
        String url = "";
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                File file = new File("");
                writeFile(file,response);
            }
        });
    }
    private void writeFile(File file,Response response) {
        OutputStream outputStream = null;
        InputStream inputStream = response.body().byteStream();
        try {
            outputStream = new FileOutputStream(file);
            int len = 0;
            byte[] buffer = new byte[1024*10];
            while ((len = inputStream.read(buffer))!=-1){
                outputStream.write(buffer,0,len);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(inputStream != null)
                    inputStream.close();
                if(outputStream != null)
                    outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

作者：cao苗子
链接：https://www.jianshu.com/p/708604084baa
来源：简书
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。