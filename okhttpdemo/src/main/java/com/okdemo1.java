package com;

import okhttp3.*;
import okio.BufferedSink;
import sun.rmi.runtime.Log;

import java.io.*;
import java.util.*;

public class okdemo1 {

    //    -new OkHttpClient;
//-构造Request对象；
//            -通过前两步中的对象构建Call对象；
//            -通过Call#enqueue(Callback)方法来提交异步请求；
    public String getdemo1Async() {
        String url = "http://wwww.baidu.com";

        OkHttpClient okHttpClient = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .get()//默认就是GET请求，可以不写
                .build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // Log.d(TAG, "onFailure: ");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                //  Log.d(TAG, "onResponse: " + response.body().string());
                System.out.println(response.body().toString());
            }
        });
        return "okHttpClient";
    }

    //1.2. 同步GET请求
    public String getdemo2() {
        String url = "http://wwww.baidu.com";
        OkHttpClient okHttpClient = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .build();
        final Call call = okHttpClient.newCall(request);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Response response = call.execute();
                    System.out.println("run: " + response.body().string());
                    //   Log.d(TAG, "run: " + response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        return "";
    }

//  https://www.jianshu.com/p/da4a806e599b
    public  void postdemo(){
        MediaType mediaType = MediaType.parse("text/x-markdown; charset=utf-8");
        String requestBody = "I am Jdqm.";
        Request request = new Request.Builder()
                .url("https://api.github.com/markdown/raw")
                .post(RequestBody.create(mediaType, requestBody))
                .build();
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
               // Log.d(TAG, "onFailure: " + e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
             //   Log.d(TAG, response.protocol() + " " +response.code() + " " + response.message());
                Headers headers = response.headers();
                for (int i = 0; i < headers.size(); i++) {
                 //   Log.d(TAG, headers.name(i) + ":" + headers.value(i));
                }
               // Log.d(TAG, "onResponse: " + response.body().string());
            }
        });

    }
private void ok() {
        OkHttpClient okHttpClient = new OkHttpClient();

        Request request = new Request.Builder().url(apk).build();

        okHttpClient.newCall(request).enqueue(new Callback() {

            private int progess;

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                // 下载 outputStream inputStream
                InputStream inputStream = response.body().byteStream();
                //文件的总长度
                long max = response.body().contentLength();
                String path = Environment.getExternalStorageDirectory() + File.separator + "banmi_330.apk";
                File file = new File(path);

                //当文件不存在，创建出来
                if (!file.exists()) {
                    file.createNewFile();
                }

                FileOutputStream fileOutputStream = new FileOutputStream(file);

                byte[] bytes = new byte[1024];

                int readLength = 0;
                long cureeLength = 0;

                while ((readLength = inputStream.read(bytes)) != -1) {
                    fileOutputStream.write(bytes, 0, readLength);

                    cureeLength += readLength;

                    progess = (int) (cureeLength * 100 / max);
                    progressBar.setProgress(progess);
                    Log.d(TAG, "onResponse: " + progess + "%");
                }

                inputStream.close();
                fileOutputStream.close();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "下载成功", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    public  void postdemo2(){
        RequestBody requestBody = new RequestBody() {
            //@Nullable
            @Override
            public MediaType contentType() {
                return MediaType.parse("text/x-markdown; charset=utf-8");
            }

            @Override
            public void writeTo(BufferedSink sink) throws IOException {
                sink.writeUtf8("I am Jdqm.");
            }
        };

        Request request = new Request.Builder()
                .url("https://api.github.com/markdown/raw")
                .post(requestBody)
                .build();
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
             //   Log.d(TAG, "onFailure: " + e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
              //  Log.d(TAG, response.protocol() + " " +response.code() + " " + response.message());
                Headers headers = response.headers();
                for (int i = 0; i < headers.size(); i++) {
              //      Log.d(TAG, headers.name(i) + ":" + headers.value(i));
                }
               // Log.d(TAG, "onResponse: " + response.body().string());
            }
        });

    }


}
//POST提交文件
//        MediaType mediaType = MediaType.parse("text/x-markdown; charset=utf-8");
//        OkHttpClient okHttpClient = new OkHttpClient();
//        File file = new File("test.md");
//        Request request = new Request.Builder()
//        .url("https://api.github.com/markdown/raw")
//        .post(RequestBody.create(mediaType, file))
//        .build();
//        okHttpClient.newCall(request).enqueue(new Callback() {
//@Override
//public void onFailure(Call call, IOException e) {
//        Log.d(TAG, "onFailure: " + e.getMessage());
//        }
//
//@Override
//public void onResponse(Call call, Response response) throws IOException {
//        Log.d(TAG, response.protocol() + " " +response.code() + " " + response.message());
//        Headers headers = response.headers();
//        for (int i = 0; i < headers.size(); i++) {
//        Log.d(TAG, headers.name(i) + ":" + headers.value(i));
//        }
//        Log.d(TAG, "onResponse: " + response.body().string());
//        }
//        });
//        2.4. POST方式提交表单
//        OkHttpClient okHttpClient = new OkHttpClient();
//        RequestBody requestBody = new FormBody.Builder()
//        .add("search", "Jurassic Park")
//        .build();
//        Request request = new Request.Builder()
//        .url("https://en.wikipedia.org/w/index.php")
//        .post(requestBody)
//        .build();
//
//        okHttpClient.newCall(request).enqueue(new Callback() {
//@Override
//public void onFailure(Call call, IOException e) {
//        Log.d(TAG, "onFailure: " + e.getMessage());
//        }
//
//@Override
//public void onResponse(Call call, Response response) throws IOException {
//        Log.d(TAG, response.protocol() + " " +response.code() + " " + response.message());
//        Headers headers = response.headers();
//        for (int i = 0; i < headers.size(); i++) {
//        Log.d(TAG, headers.name(i) + ":" + headers.value(i));
//        }
//        Log.d(TAG, "onResponse: " + response.body().string());
//        }
//        });
