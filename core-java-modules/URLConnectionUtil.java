/**
 * upload_continued.com.keeley.core - the name of the target package where the new class or interface will be created.
 * KDownload-master - the name of the current project.
 * null.java - the name of the PHP file that will be created.
 * URLConnectionUtil - the name of the new file which you specify in the New File dialog box during the file creation.
 * Administrator wei.zhou@macrowing.com - the login name of the current user.
 * 2022/8/110:44
 **/
package upload_continued.com.keeley.core;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

//https://www.cnblogs.com/greywolf/archive/2012/08/18/2645179.html
public class URLConnectionUtil {

    private static final String SERVLET_POST = "POST";
    private static final String SERVLET_GET = "GET";
    private static final String SERVLET_DELETE = "DELETE";
    private static final String SERVLET_PUT = "PUT";

    private static String prepareParam(Map<String, Object> paramMap) {
        StringBuffer sb = new StringBuffer();
        if (paramMap.isEmpty()) {
            return "";
        } else {
            for (String key : paramMap.keySet()) {
                String value = (String) paramMap.get(key);
                if (sb.length() < 1) {
                    sb.append(key).append("=").append(value);
                } else {
                    sb.append("&").append(key).append("=").append(value);
                }
            }
            return sb.toString();
        }
    }

    public static String doPostString(String urlStr, String paramStr) throws Exception {
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod(SERVLET_POST);
        //  String paramStr = prepareParam(paramMap);
        conn.setRequestProperty("Content-Type", "application/json; utf-8");
        conn.setRequestProperty("Accept", "application/json");
        conn.setDoInput(true);
        conn.setDoOutput(true);
        OutputStream os = conn.getOutputStream();
        os.write(paramStr.toString().getBytes("utf-8"));
        os.close();

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        String result = "";
        while ((line = br.readLine()) != null) {
            result += line;
        }
        System.out.println(result);
        br.close();
        return result;
    }

    public static String doPost(String urlStr, Map<String, Object> paramMap) throws Exception {
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod(SERVLET_POST);
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; utf-8");
        // conn.setRequestProperty("Accept", "application/json");
        String paramStr = prepareParam(paramMap);
        conn.setDoInput(true);
        conn.setDoOutput(true);
        OutputStream os = conn.getOutputStream();
        os.write(paramStr.toString().getBytes("utf-8"));
        os.close();

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        String result = "";
        while ((line = br.readLine()) != null) {
            result += line;
        }
        System.out.println(result);
        br.close();
        return result;
    }

    public static String doGet(String urlStr) throws Exception {
//        String paramStr = prepareParam(paramMap);
//        if (paramStr == null || paramStr.trim().length() < 1) {
//
//        } else {
//            urlStr += "?" + paramStr;
//        }
//        System.out.println(urlStr);
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod(SERVLET_GET);
      //  conn.setRequestProperty("Content-Type", "text/html; charset=UTF-8");

        conn.connect();
        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        String result = "";
        while ((line = br.readLine()) != null) {
            result +=  line;
        }
        System.out.println(result);
        br.close();
        return result;
    }

    public static void doPut(String urlStr, Map<String, Object> paramMap) throws Exception {
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod(SERVLET_PUT);
        String paramStr = prepareParam(paramMap);
        conn.setDoInput(true);
        conn.setDoOutput(true);
        OutputStream os = conn.getOutputStream();
        os.write(paramStr.toString().getBytes("utf-8"));
        os.close();

        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        String result = "";
        while ((line = br.readLine()) != null) {
            result += "/n" + line;
        }
        System.out.println(result);
        br.close();

    }

    public static void doDelete(String urlStr, Map<String, Object> paramMap) throws Exception {
        String paramStr = prepareParam(paramMap);
        if (paramStr == null || paramStr.trim().length() < 1) {

        } else {
            urlStr += "?" + paramStr;
        }
        System.out.println(urlStr);
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod(SERVLET_DELETE);
        //屏蔽掉的代码是错误的，java.net.ProtocolException: HTTP method DELETE doesn't support output
/*      OutputStream os = conn.getOutputStream();
        os.write(paramStr.toString().getBytes("utf-8"));
        os.close();  */

        if (conn.getResponseCode() == 200) {
            System.out.println("成功");
        } else {
            System.out.println(conn.getResponseCode());
        }
    }

//    public   static   void  main(String[] args)  throws  Exception{
//        String urlStr = "http://localhost:8080/SwTest/ReceiveDataServlet" ;
//        Map<String,Object> map = new  HashMap<String,Object>();
//        map.put("username" , "张三" );
//        map.put("password" , "88888" );
////      URLConnectionUtil.doGet(urlStr, map);
////      URLConnectionUtil.doPost(urlStr, map);
////      URLConnectionUtil.doPut(urlStr, map);
//        URLConnectionUtil.doDelete(urlStr, map);
//
//    }


}