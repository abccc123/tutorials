package com.example.demo;

import org.slf4j.ILoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@EnableAutoConfiguration
@SpringBootApplication
public class DemoApplication {

	//定义一个全局的记录器，通过LoggerFactory获取
	private final static Logger logger = LoggerFactory.getLogger(DemoApplication.class);
	public static void main(String[] args) {
//		boolean debug = args.con("debug");
//		List<String> files = args.getNonOptionArgs();
//		if (debug) {
//			System.out.println(files);
//		}
		logger.info("-----------");
		SpringApplication.run(DemoApplication.class, args);
	}
	@RequestMapping("/Hello")
	String home() {
		return "Hello World!";
	}

}
