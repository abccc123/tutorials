package com.example.demo.restservice;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

import com.example.demo.DemoApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@Profile("dev")
@RestController
public class GreetingController {

    private final static Logger logger = LoggerFactory.getLogger(GreetingController.class);
    RestTemplate restTemplate = new RestTemplate();
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    //http://localhost:8080/greeting?name=3wwwwwww2&World=dddddddd
    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

    //http://localhost:8080/greeting1/1
    @GetMapping("/{id}")
    public Greeting greeting1(@PathVariable Integer id) {
        return new Greeting(counter.incrementAndGet(), String.format(template, "name"));
    }

    @PostMapping("/update/{id}")
    public Greeting update(@PathVariable Integer id) {
        return new Greeting(counter.incrementAndGet(), String.format(template, "name"));
    }

    @GetMapping("/test")
    public Greeting test(@RequestParam(value = "name", defaultValue = "World") String name) {
//        HashMap<String,String> list=  new HashMap<String,String>();
//        restTemplate.postForEntity("",list,Greeting.class);

        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

    @GetMapping("/upload")
    public Greeting upload(@RequestParam(value = "name", defaultValue = "World") String name) {
        String token = "0034d80e69ad025c49ceb04d98b9751a2db5";
        name = UUID.randomUUID() + ".txt";
// 设置请求头
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/x-www-form-urlencoded");
// 设置请求参数
        MultiValueMap<String, Object> formData = new LinkedMultiValueMap();
        formData.add("token", token);
        formData.add("module", "RegionDocOperationApi");
        formData.add("fun", "CheckAndCreateDocInfo");
        formData.add("fileModel", "UPLOAD");
        formData.add("filename", name);
        formData.add("size", 6666);
        formData.add("folderId", 30);

        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(formData, headers);


        ResponseEntity<String> responseEntity = this.restTemplate.exchange("http://192.168.251.155/webcore?token=" + token, HttpMethod.POST, httpEntity, String.class, new Object[0]);
        logger.info("upload  filename " + name + "   " + (String) responseEntity.getBody());

        return new Greeting(counter.incrementAndGet(), String.format("%s", (String) responseEntity.getBody()));
    }

    /*http://localhost:8766/upload2
     * */
    @GetMapping("/upload2")
    public Greeting upload2(@RequestParam(value = "name", defaultValue = "World") String name) {
        String token = "0034d80e69ad025c49ceb04d98b9751a2db5";
        name = UUID.randomUUID() + ".txt";
// 设置请求头
        HttpHeaders headers = new HttpHeaders();
        //   headers.add("Content-Type", "application/x-www-form-urlencoded");
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
// 设置请求参数
        MultiValueMap<String, Object> formData = new LinkedMultiValueMap();
        formData.add("token", token);
        formData.add("module", "RegionDocOperationApi");
        formData.add("fun", "CheckAndCreateDocInfo");
        formData.add("fileModel", "UPLOAD");
        formData.add("filename", name);
        formData.add("size", 6666);
        formData.add("folderId", 30);

        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(formData, headers);


        ResponseEntity<String> responseEntity = this.restTemplate.exchange("http://192.168.251.155/webcore?token=" + token, HttpMethod.POST, httpEntity, String.class, new Object[0]);
        logger.info("upload  filename " + name + "   " + (String) responseEntity.getBody());

        return new Greeting(counter.incrementAndGet(), String.format("%s", (String) responseEntity.getBody()));
    }

    /*
     * http://localhost:8766/dev/httpjson?name=1
     * */
    @GetMapping("/httpjson")
    public Greeting httpjson(@RequestParam(value = "name", defaultValue = "World") String name) throws JSONException {
        JSONObject json = new JSONObject();
        json.put("userName", "userName");
        json.put("clientType", "4");
        json.put("password", "password");

        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        HttpEntity<String> httpEntity = new HttpEntity<String>(json.toString(), headers);

        ResponseEntity<String> responseEntity = this.restTemplate.exchange("https://vxx.com/api/services/Org/UserLogin", HttpMethod.POST, httpEntity, String.class, new Object[0]);
        logger.info("upload  filename " + name + "   " + (String) responseEntity.getBody());
        return new Greeting(counter.incrementAndGet(), String.format("%s", (String) responseEntity.getBody()));
        // return new Greeting(counter.incrementAndGet(), String.format("%s", ""));
    }
}