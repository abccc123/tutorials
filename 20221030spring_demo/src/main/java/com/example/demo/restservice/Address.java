package com.example.demo.restservice;
import org.springframework.stereotype.Component;

@Component
public class Address {    private  String username;

//    public Address(String username) {
//        this.username = username;
//    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
