package com.example.demo.restservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.List;
 //  ConfigurationProperties 用于yaml 和bean 绑定
//https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.external-config.yaml
@Component
//@ConfigurationProperties(prefix = "user") //前缀
public class User {
@Value("${user.username}")
    private  String username;
    @Value("${user.age}")
    private  Integer age;
    private Date brith;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getBrith() {
        return brith;
    }

    public void setBrith(Date brith) {
        this.brith = brith;
    }
//
//    public List<String> getAddlist() {
//        return addlist;
//    }
//
//    public void setAddlist(List<String> addlist) {
//        this.addlist = addlist;
//    }
//
//    private List<String> addlist;

}
