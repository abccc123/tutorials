package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc  // mockmvc
class DemoApplicationTests {
@Autowired
	MockMvc mockMvc;
	@Autowired
	com.example.demo.restservice.User usr;

	@Test
	public 	void GetcrestserviceUser() throws Exception {
		System.out.printf("  "+ usr.getUsername());
	}
	@Test
public 	void GetcontextLoads() throws Exception {
	mockMvc.perform(MockMvcRequestBuilders.get("/user/{id}").accept(MediaType.APPLICATION_NDJSON)
					.param("id","123"))
			.andExpect(MockMvcResultMatchers.status().isOk())//200
			.andExpect(MockMvcResultMatchers.jsonPath("$.data.result").value("123"))//result ==123
			.andDo(MockMvcResultHandlers.print());
	}
	@Test
	public 	void PosttcontextLoads() throws Exception {
		// alt+ enter
		String user= "{\n" +
				"  \"name\": \"wwww\",\n" +
				"  \"age\": 234,\n" +
				"  \"dd\": \"dddddddd\"\n" +
				"}";

		mockMvc.perform(MockMvcRequestBuilders.get("/user/{id}").accept(MediaType.APPLICATION_NDJSON)
						.contentType(MediaType.parseMediaType("application/json"))
						.content(user)
						.param("id","123"))
				.andExpect(MockMvcResultMatchers.status().isOk())//200
				.andExpect(MockMvcResultMatchers.jsonPath("$.data.result").value("123"))//result ==123
				.andDo(MockMvcResultHandlers.print());
	}
}
