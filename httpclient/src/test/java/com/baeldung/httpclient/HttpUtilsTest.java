package com.baeldung.httpclient;

import com.alibaba.fastjson2.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.*;

public class HttpUtilsTest {
    private HttpUtils response;

    @Before
    public final void before() {
        response =new HttpUtils();
    }

    /*
    * 测试不通过*/
    @Test
    public   void doPostJsonParam() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        String result = "";

        // 测试 https
        String urlHttps = "https://v5.edoc2.com";
        String secretKeyHttps = "my-secret-key";
        CloseableHttpClient client= new HttpsClient2().getCloseableHttpsClient();
        // 构造 request
        HttpGet getRequest_1 = new HttpGet(urlHttps);
        getRequest_1.setHeader("secretKey", secretKeyHttps);
        // 配置 request
        RequestConfig requestConfig = RequestConfig.custom()
                // 设置连接超时时间(单位毫秒)
                .setConnectTimeout(120000)
                // 设置请求超时时间(单位毫秒)
                .setConnectionRequestTimeout(120000)
                // socket读写超时时间(单位毫秒)
                .setSocketTimeout(120000)
                // 设置是否允许重定向(默认为true)
                .setRedirectsEnabled(true).build();
        getRequest_1.setConfig(requestConfig);
        try {
            CloseableHttpResponse  response =  client.execute(getRequest_1);

            HttpEntity responseEntity = response.getEntity();
            if (responseEntity != null) {
                result =responseEntity.getContent().toString();
                System.out.println("result: " + result);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public  void doGet() throws Exception {
        String w=  HttpUtils.doGet("https://v5.edoc2.com",null, "");
        String a=" d";
    }
/*ok*/
    @Test
    public  void httpPost() throws Exception {
//        StringEntity s = new StringEntity("{\n" +
//                "  \"userName\": \"aa\",\n" +
//                "  \"password\": \"password\",\n" +
//
//                "  \"secure\": false\n" +
//                "}", "utf-8");
//        s.setContentEncoding("utf-8");
//        s.setContentType("application/json");
        com.alibaba.fastjson2.JSONObject jsonObject=new  com.alibaba.fastjson2.JSONObject();
        jsonObject.put("userName","userName");
        jsonObject.put("password","password");
        jsonObject.put("secure","false");
        String w=  HttpUtils.doPostJsonParam("https://v76.com/api/services/Org/UserLogin",jsonObject);
        String a=" d";
    }
}