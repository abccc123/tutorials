/**
 * com.baeldung.httpclient - the name of the target package where the new class or interface will be created.
 * httpclient - the name of the current project.
 * null.java - the name of the PHP file that will be created.
 * HttpRespons - the name of the new file which you specify in the New File dialog box during the file creation.
 * Administrator wei.zhou@macrowing.com - the login name of the current user.
 * 2022/11/1223:03
 **/
package com.baeldung.httpclient;


import java.util.Vector;

public class HttpRespons {
    public Vector<String> contentCollection;

    public String urlString;

    public int defaultPort;

    public String file;

    public String host;

    public String path;

    public int port;

    public String protocol;

    public String query;

    public String ref;

    public String userInfo;

    public String content;

    public String contentEncoding;

    public int code;

    public String message;

    public String contentType;

    public String method;

    public int connectTimeout;

    public int readTimeout;
}