package com.baeldung.httpclient;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.sql.Clob;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**

 */
public class StringHelper {
    public static String STR_SPLIT = ",";
    public static String STR_EMPTY = "";
    public static String STR_TRUE = "TRUE";
    public static String STR_FALSE = "FALSE";
    public static String CHECKBOX_ON = "on";
    public static String CHECKBOX_OFF = "off";
    public static String CHECKBOX_TRUE = "true";
    public static String CHECKBOX_FALSE = "false";
    //下面是字符串，流、字节流之间的相互转换
    static int BUFFER_SIZE = 4096; //缓冲区大小
    private static Map<String, Object> map = new HashMap<String, Object>();

    /**
     * 判断字符串是否是整数
     *
     * @param number
     * @return
     */
    public static boolean isInteger(String number) {
        boolean isNumber = false;
        if (StringHelper.isNotEmpty(number)) {
            isNumber = number.matches("^([1-9]\\d*)|(0)$");
        }
        return isNumber;
    }

    /**
     * @param str
     * @return
     */
    public static String encodeStr(String str) {
        if (StringHelper.isNotEmpty(str))
            return new String(str.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
        else
            return "";
    }

    /**
     * 判断是否为空
     *
     * @param str
     * @return
     */
    public static boolean isNotEmpty(Object str) {
        boolean isEmpty = true;
        if (str == "null" || str == null || "".equals(str.toString())) {
            isEmpty = false;
        }
        return isEmpty;
    }

    /**
     * 讲数组转成SQL认识的字符串
     *
     * @param ids
     * @return
     */
    public static String fromArrayToStr(String[] ids) {
        StringBuffer idSql = new StringBuffer();
        if (null != ids && 0 != ids.length) {
            int i = 0;
            while (i < ids.length) {
                idSql.append("'");
                idSql.append(ids[i]);
                idSql.append("'");
                i++;
                if (i != ids.length) {
                    idSql.append(STR_SPLIT);
                }
            }
        } else {
            idSql.append("'NULL'");
        }
        return idSql.toString();
    }

    /**
     * 将字符串转化为INT类型
     *
     * @param string
     * @return
     */
    public static Integer toInteger(String string) {
        if (string != null && !string.trim().equals(STR_EMPTY)) {
            return Integer.parseInt(string);
        }
        return 0;
    }

    /**
     * 转化POJO为String
     *
     * @param obj
     * @return
     */
    public static String asString(Object obj) {
        return obj != null ? obj.toString() : "";
    }

    /**
     * 替换checkbox的值
     *
     * @param chekcBox
     * @return
     */
    public static String replayCheckbox(String chekcBox) {
        if (CHECKBOX_ON.equals(chekcBox)
                || CHECKBOX_TRUE.equals(chekcBox)) {
            return "1";
        } else if (CHECKBOX_OFF.equals(chekcBox)
                || CHECKBOX_FALSE.equals(chekcBox)) {
            return "0";
        } else {
            return chekcBox;
        }
    }

    /**
     * 替换成checkbox认识的值
     *
     * @param checkBox
     * @return
     */
    public static String replayToCheckBox(String checkBox) {
        if (checkBox.equals("1")) {
            return CHECKBOX_ON;
        } else {
            return CHECKBOX_OFF;
        }
    }

    /**
     * 判断是否空字符串
     *
     * @param obj
     * @return
     */
    public static boolean isEmpty(String obj) {
        return ("".equals(obj) || obj == null);
    }

    /**
     * 左侧用字符串补足到一定长度
     *
     * @param srcString
     * @param padded_length
     * @param paddingString
     * @return
     */
    public static String lpad(String srcString, int padded_length,
                              String paddingString) {
        int srcLength = asString(srcString).length();
        int padStrLength = asString(paddingString).length();

        if (srcLength < padded_length && !isEmpty(paddingString)
                && !isEmpty(paddingString)) {
            int leftLength = padded_length - srcLength;
            int cnt = leftLength / padStrLength
                    + (leftLength % padStrLength > 0 ? 1 : 0);
            StringBuffer sb = new StringBuffer(padStrLength * cnt);
            for (int i = 0; i < cnt; i++) {
                sb.append(paddingString);
            }
            return sb.substring(0, leftLength) + srcString;
        } else if (srcLength > padded_length) {
            return srcString.substring(0, padded_length);
        } else {
            return srcString;
        }
    }

    /**
     * 右侧用字符串补足到一定长度
     *
     * @param srcString
     * @param padded_length
     * @param paddingString
     * @return
     */
    public static String rpad(String srcString, int padded_length,
                              String paddingString) {
        int srcLength = asString(srcString).length();
        int padStrLength = asString(paddingString).length();

        if (srcLength < padded_length && !isEmpty(paddingString)
                && !isEmpty(paddingString)) {
            int leftLength = padded_length - srcLength;
            int cnt = leftLength / padStrLength
                    + (leftLength % padStrLength > 0 ? 1 : 0);
            StringBuffer sb = new StringBuffer(padStrLength * cnt);
            for (int i = 0; i < cnt; i++) {
                sb.append(paddingString);
            }
            return srcString + sb.substring(0, leftLength);
        } else if (srcLength > padded_length) {
            return srcString.substring(0, padded_length);
        } else {
            return srcString;
        }
    }

    /**
     * 将字符串数组链接成字符串
     *
     * @param list
     * @param joinStr
     * @return
     */
    public static String join(String[] list, String joinStr) {
        StringBuffer s = new StringBuffer();
        for (int i = 0; list != null && i < list.length; i++) {
            if ((i + 1) == list.length) {
                s.append(list[i]);
            } else {
                s.append(list[i]).append(joinStr);
            }
        }
        return s.toString();
    }

    /**
     * 第一个字母转化成小写
     *
     * @param s
     * @return
     */
    public static String firstCharLowerCase(String s) {
        if (s == null || "".equals(s)) {
            return ("");
        }
        return s.substring(0, 1).toLowerCase() + s.substring(1);
    }

    /**
     * 第一个字符转化成大写
     *
     * @param s
     * @return
     */
    public static String firstCharUpperCase(String s) {
        if (s == null || "".equals(s)) {
            return ("");
        }
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    /**
     * 将带下划线的字符串转换成驼峰命名法的变量  例如：将bss_org_id转换成bssOrgId
     *
     * @param src
     * @return
     */
    public static String toBeanPatternStr(String src) {
        String dist = src.toLowerCase();
        Pattern pattern = Pattern.compile("_([a-z0-9])");
        Matcher matcher = pattern.matcher(dist);
        while (matcher.find()) {
            dist = dist.replaceFirst(matcher.group(0), matcher.group(1)
                    .toUpperCase());
        }
        return dist;
    }

    /**
     * 把所有的回车换行符换成"\n"，避免页面上的Javascript错误
     *
     * @param src
     * @return
     */
    public static String toJSLineSeparateStr(String src) {
        if (src == null) {
            return "";
        }
        String dist = src;
        dist = dist.replaceAll("\r\n", "\\\\n");
        dist = dist.replaceAll("\r", "\\\\n");
        dist = dist.replaceAll("\n", "\\\\n");
        return dist;
    }

    /**
     * 截取指定长度字符串
     *
     * @param input
     * @param tail
     * @param length
     * @return
     */
    public static String getShorterString(String input, String tail, int length) {
        tail = isEmpty(tail) ? tail : "";
        StringBuffer buffer = new StringBuffer(512);
        try {
            int len = input.getBytes("GBK").length;
            if (len > length) {
                int ln = 0;
                for (int i = 0; ln < length; i++) {
                    String temp = input.substring(i, i + 1);
                    if (temp.getBytes("GBK").length == 2) {
                        ln += 2;
                    } else {
                        ln++;
                    }
                    if (ln <= length) {
                        buffer.append(temp);
                    }
                }
            } else {
                return input;
            }
            buffer.append(tail);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return buffer.toString();
    }

    /**
     * 转换字符串编码 取得GBK编码
     *
     * @param input
     * @param code
     * @return
     */
    public static String getBytesString(String input, String code) {
        try {
            byte[] b = input.getBytes(code);
            return Arrays.toString(b);
        } catch (UnsupportedEncodingException e) {
            return String.valueOf(code.hashCode());
        }
    }

    /**
     * 读取ORACLE的CLOB，转化为String
     *
     * @param clob
     * @return
     */
    public static String getStringFromClob(Clob clob) {
        String result = "";
        try {
            if (clob == null) {
                return null;
            }
            Reader reader = clob.getCharacterStream();// 得到流
            BufferedReader br = new BufferedReader(reader);
            String line = br.readLine();
            StringBuffer sb = new StringBuffer(1024);
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            result = sb.toString();
        } catch (Exception ex) {

        }
        return result;
    }

    /**
     * 将INPUTSTREAM转换成字符串
     *
     * @param inputStream
     * @return
     */
    public static String InputStreamTOString(InputStream inputStream) {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] data = new byte[4096];
        String string = null;
        int count = 0;
        try {
            while ((count = inputStream.read(data, 0, 4096)) != -1) {
                outStream.write(data, 0, count);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        data = null;
        string = new String(outStream.toByteArray(), StandardCharsets.UTF_8);
        return string;
    }

    /**
     * 将INPUTSTREAM转换成字符串
     *
     * @param inputStream
     * @param encoding
     * @return
     */
    public static String InputStreamTOString(InputStream inputStream, String encoding) {
        String string = null;
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] data = new byte[4096];
        int count = -1;
        try {
            while ((count = inputStream.read(data, 0, 4096)) != -1)
                outStream.write(data, 0, count);
        } catch (IOException e) {
            e.printStackTrace();
        }

        data = null;
        try {
            string = new String(outStream.toByteArray(), encoding);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return string;
    }

    /**
     * 将字符串转换为INPUTSTREAM
     *
     * @param string
     * @return
     * @throws Exception
     */
    public static InputStream StringTOInputStream(String string) throws Exception {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8));
        return inputStream;
    }

    /**
     * 将字符串转换成字节数组
     *
     * @param string
     * @return
     */
    public static byte[] StringTObyte(String string) {
        byte[] bytes = null;
        try {
            bytes = InputStreamTOByte(StringTOInputStream(string));
        } catch (IOException localIOException) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bytes;
    }

    /**
     * 将INPUTSTREAM转换成字节数组
     *
     * @param inputStream
     * @return
     * @throws IOException
     */
    public static byte[] InputStreamTOByte(InputStream inputStream) throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] data = new byte[4096];
        int count = -1;
        while ((count = inputStream.read(data, 0, 4096)) != -1) {
            outStream.write(data, 0, count);
        }
        data = null;
        return outStream.toByteArray();
    }


    /**
     * byte数组转字符串
     *
     * @param bytes
     * @return
     */
    public static String byteTOString(byte[] bytes) {
        InputStream is = null;
        try {
            is = byteToInputStream(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return InputStreamTOString(is, "UTF-8");
    }

    /**
     * 获取字符串
     *
     * @param string
     * @return
     */
    public static String getString(String string) {
        String is = null;
        try {
            is = byteTOString(StringTObyte(string));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return is;
    }

    /**
     * 将byte数组转化为输入流
     *
     * @param bytes
     * @return
     */
    public static InputStream byteToInputStream(byte[] bytes) {
        ByteArrayInputStream is = new ByteArrayInputStream(bytes);
        return is;
    }

    /**
     * 根据路径获取文件输入流
     *
     * @param filepath
     * @return
     */
    public static FileInputStream getFileInputStream(String filepath) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(filepath);
        } catch (FileNotFoundException e) {
            System.out.print("错误信息:文件不存在");
            e.printStackTrace();
        }
        return fileInputStream;
    }

    /**
     * 获取文件输入流
     *
     * @param file
     * @return
     */
    public static FileInputStream getFileInputStream(File file) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            System.out.print("错误信息:文件不存在");
            e.printStackTrace();
        }
        return fileInputStream;
    }

    /**
     * 获取文件输出流
     *
     * @param file
     * @param append
     * @return
     */
    public static FileOutputStream getFileOutputStream(File file, boolean append) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file, append);
        } catch (FileNotFoundException e) {
            System.out.print("错误信息:文件不存在");
            e.printStackTrace();
        }
        return fileOutputStream;
    }

    /**
     * 根据路径获取文件输出流
     *
     * @param filepath
     * @param append
     * @return
     */
    public static FileOutputStream getFileOutputStream(String filepath, boolean append) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(filepath, append);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return fileOutputStream;
    }

    /**
     * 根据路径获取文件
     *
     * @param filepath
     * @return
     */
    public static File getFile(String filepath) {
        return new File(filepath);
    }

    /**
     * 获取字节数组输出流
     *
     * @return
     */
    public static ByteArrayOutputStream getByteArrayOutputStream() {
        return new ByteArrayOutputStream();
    }

    /**
     * 获取map
     *
     * @return
     */
    public static Map<String, Object> getMap() {
        return map;
    }

    /**
     * 判断对象为空
     *
     * @param object
     * @return
     */
    public static boolean isEmpty(Object object) {
        if (object == null) {
            return (true);
        }
        if (object.equals("")) {
            return (true);
        }
        return object.equals("null");
    }

    /**
     * 字符串解码
     *
     * @param strIn
     * @param sourceCode
     * @param targetCode
     * @return
     */
    public static String decode(String strIn, String sourceCode, String targetCode) {
        String temp = codeToOtherCode(strIn, sourceCode, targetCode);
        return temp;
    }

    /**
     * 将字符串转化为UTF-8
     *
     * @param strIn
     * @return
     */
    public static String StrToUTF8(String strIn) {
        strIn = "";
        strIn = new String(strIn.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
        return strIn;
    }

    /**
     * 将字符串从一种编码转到另外一种
     *
     * @param strIn
     * @param sourceCode
     * @param targetCode
     * @return
     */
    private static String codeToOtherCode(String strIn, String sourceCode, String targetCode) {
        String strOut = null;
        if (strIn == null || (strIn.trim()).equals("")) {
            return strIn;
        }
        try {
            byte[] b = strIn.getBytes(sourceCode);
            strOut = new String(b, targetCode);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return strOut;
    }

    /**
     * 将字符串转化为INT
     *
     * @param string
     * @param defval
     * @return
     */
    public static int getInt(String string, int defval) {
        if (string == null || string == "") {
            return (defval);
        }
        try {
            return (Integer.parseInt(string));
        } catch (NumberFormatException e) {
            return (defval);
        }
    }

    /***
     * 将字符串转化为INT
     * @param string
     * @return
     */
    public static int getInt(String string) {
        if (string == null || string == "") {
            return 0;
        }
        try {
            return (Integer.parseInt(string));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    /**
     * 将字符串转化为INT
     *
     * @param s
     * @param df
     * @return
     */
    public static int getInt(String s, Integer df) {
        if (s == null || s == "") {
            return df;
        }
        try {
            return (Integer.parseInt(s));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    /**
     * 将字符串数组转化为INT数组
     *
     * @param string
     * @return
     */
    @SuppressWarnings("unused")
    public static Integer[] getInts(String[] string) {
        Integer[] integer = new Integer[string.length];
        if (string == null) {
            return null;
        }
        for (int i = 0; i < string.length; i++) {
            integer[i] = Integer.parseInt(string[i]);
        }
        return integer;

    }

    /**
     * 将字符串转化为双精度型
     *
     * @param string
     * @param defval
     * @return
     */
    public static double getDouble(String string, double defval) {
        if (string == null || string == "") {
            return (defval);
        }
        try {
            return (Double.parseDouble(string));
        } catch (NumberFormatException e) {
            return (defval);
        }
    }

    /**
     * 将双精度数转化为双精度
     *
     * @param doub
     * @param defval
     * @return
     */
    public static double getDou(Double doub, double defval) {
        if (doub == null) {
            return (defval);
        }
        return doub;
    }

    /**
     * 将字符串转化为短整形
     *
     * @param string
     * @return
     */
    public static Short getShort(String string) {
        if (StringHelper.isNotEmpty(string)) {
            return (Short.parseShort(string));
        } else {
            return null;
        }
    }

    /**
     * 将对象转化为int
     *
     * @param object
     * @param defval
     * @return
     */
    public static int getInt(Object object, int defval) {
        if (isEmpty(object)) {
            return (defval);
        }
        try {
            return (Integer.parseInt(object.toString()));
        } catch (NumberFormatException e) {
            return (defval);
        }
    }

    /**
     * 将BIGDECIMAL转换成整形
     *
     * @param bigDecimal
     * @param defval
     * @return
     */
    public static int getInt(BigDecimal bigDecimal, int defval) {
        if (bigDecimal == null) {
            return (defval);
        }
        return bigDecimal.intValue();
    }

    /**
     * 将字符串数组转化为int数组
     *
     * @param object
     * @return
     */
    public static Integer[] getIntegerArray(String[] object) {
        int len = object.length;
        Integer[] result = new Integer[len];
        try {
            for (int i = 0; i < len; i++) {
                result[i] = new Integer(object[i].trim());
            }
            return result;
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /**
     * 去顶字符串的空格
     *
     * @param s
     * @return
     */
    public static String getStrings(String s) {
        return (getString(s, ""));
    }

    /**
     * 去掉对象的空格
     *
     * @param object
     * @return
     */
    public static String getString(Object object) {
        if (isEmpty(object)) {
            return "";
        }
        return (object.toString().trim());
    }

    /**
     * 将int类型转化为string
     *
     * @param i
     * @return
     */
    public static String getString(int i) {
        return (String.valueOf(i));
    }

    /**
     * 将浮点型转化为string
     *
     * @param f
     * @return
     */
    public static String getString(float f) {
        return (String.valueOf(f));
    }

    /**
     * 字符串去掉空格
     *
     * @param string
     * @param defval
     * @return
     */
    public static String getString(String string, String defval) {
        if (isEmpty(string)) {
            return (defval);
        }
        return (string.trim());
    }

    /**
     * 对象去掉空格
     *
     * @param s
     * @param defval
     * @return
     */
    public static String getString(Object s, String defval) {
        if (isEmpty(s)) {
            return (defval);
        }
        return (s.toString().trim());
    }

    /**
     * 将字符串转化为长整型
     *
     * @param str
     * @return
     */
    public static long stringToLong(String str) {
        Long test = new Long(0);
        try {
            test = Long.valueOf(str);
        } catch (Exception e) {
        }
        return test.longValue();
    }

    /**
     * 获取本机IP
     *
     * @return
     */
    public static String getIp() {
        String ip = null;
        try {
            InetAddress address = InetAddress.getLocalHost();
            ip = address.getHostAddress();

        } catch (UnknownHostException e) {
        }
        return ip;
    }

    /**
     * 判断一个类是否为基本数据类型
     *
     * @param clazz
     * @return
     * @throws Exception
     */
    @SuppressWarnings({"rawtypes", "unused"})
    private static boolean isBaseDataType(Class clazz) throws Exception {
        return (clazz.equals(String.class) || clazz.equals(Integer.class)
                || clazz.equals(Byte.class) || clazz.equals(Long.class)
                || clazz.equals(Double.class) || clazz.equals(Float.class)
                || clazz.equals(Character.class) || clazz.equals(Short.class)
                || clazz.equals(BigDecimal.class)
                || clazz.equals(BigInteger.class)
                || clazz.equals(Boolean.class) || clazz.equals(Date.class) || clazz.isPrimitive());
    }

    /**
     * 获取请求的IP地址
     *
     * @param request
     * @return
     */
    public static String getIpAddrByRequest(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    /**
     * 获取本机IP
     *
     * @return
     * @throws SocketException
     */
    public static String getRealIp() throws SocketException {
        String localip = null;// 本地IP，如果没有配置外网IP则返回它
        String netip = null;// 外网IP
        Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
        InetAddress ip = null;
        boolean finded = false;// 是否找到外网IP
        while (netInterfaces.hasMoreElements() && !finded) {
            NetworkInterface ni = netInterfaces.nextElement();
            Enumeration<InetAddress> address = ni.getInetAddresses();
            while (address.hasMoreElements()) {
                ip = address.nextElement();
                if (!ip.isSiteLocalAddress() && !ip.isLoopbackAddress()
                        && ip.getHostAddress().indexOf(":") == -1) {// 外网IP
                    netip = ip.getHostAddress();
                    finded = true;
                    break;
                } else if (ip.isSiteLocalAddress() && !ip.isLoopbackAddress()
                        && ip.getHostAddress().indexOf(":") == -1) {// 内网IP
                    localip = ip.getHostAddress();
                }
            }
        }
        if (netip != null && !"".equals(netip)) {
            return netip;
        } else {
            return localip;
        }
    }

    /**
     * java去除字符串中的空格、回车、换行符、制表符
     *
     * @param str
     * @return
     */
    public static String replaceBlank(String str) {
        String dest = "";
        if (str != null) {
            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
            Matcher m = p.matcher(str);
            dest = m.replaceAll("");
        }
        return dest;

    }

    /**
     * 判断元素是否在数组内
     *
     * @param substring
     * @param source
     * @return
     */
    public static boolean isInArray(String substring, String[] source) {
        if (source == null || source.length == 0) {
            return false;
        }
        for (int i = 0; i < source.length; i++) {
            String aSource = source[i];
            if (aSource.equals(substring)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取Map对象
     *
     * @return
     */
    public static Map<Object, Object> getHashMap() {
        return new HashMap<Object, Object>();
    }

    /**
     * SET转换MAP
     *
     * @param setobj
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static Map<Object, Object> SetToMap(Set<Object> setobj) {
        Map<Object, Object> map = getHashMap();
        for (Iterator iterator = setobj.iterator(); iterator.hasNext(); ) {
            Map.Entry<Object, Object> entry = (Map.Entry<Object, Object>) iterator.next();
            map.put(entry.getKey().toString(), entry.getValue() == null ? "" : entry.getValue().toString().trim());
        }
        return map;
    }

    /**
     * 是否是内网IP
     *
     * @param ipAddress
     * @return
     */
    public static boolean isInnerIP(String ipAddress) {
        boolean isInnerIp = false;
        long ipNum = getIpNum(ipAddress);
        /**
         * 私有IP：A类 10.0.0.0-10.255.255.255 B类 172.16.0.0-172.31.255.255 C类
         * 192.168.0.0-192.168.255.255 当然，还有127这个网段是环回地址
         **/
        long aBegin = getIpNum("10.0.0.0");
        long aEnd = getIpNum("10.255.255.255");
        long bBegin = getIpNum("172.16.0.0");
        long bEnd = getIpNum("172.31.255.255");
        long cBegin = getIpNum("192.168.0.0");
        long cEnd = getIpNum("192.168.255.255");
        isInnerIp = isInner(ipNum, aBegin, aEnd) || isInner(ipNum, bBegin, bEnd) || isInner(ipNum, cBegin, cEnd) || ipAddress.equals("127.0.0.1");
        return isInnerIp;
    }

    /**
     * 获取数字IP
     *
     * @param ipAddress
     * @return
     */
    private static long getIpNum(String ipAddress) {
        String[] ip = ipAddress.split("\\.");
        long a = Integer.parseInt(ip[0]);
        long b = Integer.parseInt(ip[1]);
        long c = Integer.parseInt(ip[2]);
        long d = Integer.parseInt(ip[3]);
        long ipNum = a * 256 * 256 * 256 + b * 256 * 256 + c * 256 + d;
        return ipNum;
    }

    /**
     * 是否是内网IP
     *
     * @param userIp
     * @param begin
     * @param end
     * @return
     */
    private static boolean isInner(long userIp, long begin, long end) {
        return (userIp >= begin) && (userIp <= end);
    }

    /**
     * 格式化
     *
     * @param from
     * @param to
     * @return
     * @author leejean
     * @create 2014年9月16日下午2:13:17
     */
    public static String fmtDateBetweenParams(String o_date, String from, String to) {
        if (StringHelper.isNotEmpty(from) || StringHelper.isNotEmpty(to)) {
            if (!StringHelper.isNotEmpty(from)) {
                return " and ( " + o_date + " < '" + to + "' ) ";
            }
            if (!StringHelper.isNotEmpty(to)) {
                return " and ( " + o_date + " > '" + from + "' ) ";
            }
            return " and (" + o_date + " between '" + from + "' and '" + to + "') ";
        }
        return "";
    }

    /**
     * 通过输入流获取字节数组
     *
     * @param inputStream
     * @return
     * @throws IOException
     */
    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] b = new byte[4096];
        int len = 0;
        while ((len = inputStream.read(b, 0, 4096)) != -1) {
            baos.write(b, 0, len);
        }
        baos.flush();
        byte[] bytes = baos.toByteArray();
        return bytes;
    }
}
