/**
 * com.baeldung.httpclient - the name of the target package where the new class or interface will be created.
 * httpclient - the name of the current project.
 * null.java - the name of the PHP file that will be created.
 * HttpclientOk - the name of the new file which you specify in the New File dialog box during the file creation.
 * Administrator wei.zhou@macrowing.com - the login name of the current user.
 * 2022/11/1310:41
 **/
package com.baeldung.httpclient;
import java.security.KeyManagementException;
        import java.security.KeyStoreException;
        import java.security.NoSuchAlgorithmException;
        import java.security.cert.CertificateException;
        import java.security.cert.X509Certificate;
        import javax.net.ssl.HostnameVerifier;
        import javax.net.ssl.SSLContext;
        import org.apache.http.client.HttpClient;
        import org.apache.http.client.config.RequestConfig;
        import org.apache.http.conn.ssl.NoopHostnameVerifier;
        import org.apache.http.conn.ssl.TrustStrategy;
        import org.apache.http.impl.client.CloseableHttpClient;
        import org.apache.http.impl.client.HttpClients;
        import org.apache.http.ssl.SSLContexts;
//        import org.apache.http.ssl.TrustStrategy;
/*
* ok
* */
public class HttpclientOk {
    static HttpClient _httpClient = null;

    static Object _syncLock = new Object();

    public static HttpClient CreateHttpClient() {
        RequestConfig requestConfig = null;
        if (true) {
          //  if (config.getTimeout() == 0) {
                requestConfig = RequestConfig.custom().setConnectTimeout(60000).setSocketTimeout(60000).build();
//            } else {
//                requestConfig = RequestConfig.custom().setConnectTimeout(config.getTimeout()).setSocketTimeout(config.getTimeout()).build();
//            }
            SSLContext sslContext = null;
            try {
                sslContext = SSLContexts.custom().loadTrustMaterial(null, (TrustStrategy)new TrustStrategy() {
                    public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                        return true;
                    }
                }).build();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            }
            CloseableHttpClient httpClient = HttpClients.custom().setSslcontext(sslContext).setSSLHostnameVerifier((HostnameVerifier)new NoopHostnameVerifier()).setDefaultRequestConfig(requestConfig).build();
            return (HttpClient)httpClient;
        }
        if (_httpClient == null)
            synchronized (_syncLock) {
                if (_httpClient == null) {
//                    if (config.getTimeout() == 0) {
                     requestConfig = RequestConfig.custom().setConnectTimeout(60000).setSocketTimeout(60000).build();
//                    } else {
//                        requestConfig = RequestConfig.custom().setConnectTimeout(config.getTimeout()).setSocketTimeout(config.getTimeout()).build();
//                    }
                    SSLContext sslContext = null;
                    try {
                        sslContext = SSLContexts.custom().loadTrustMaterial(null, (TrustStrategy)new TrustStrategy() {
                            public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                                return true;
                            }
                        }).build();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (KeyManagementException e) {
                        e.printStackTrace();
                    } catch (KeyStoreException e) {
                        e.printStackTrace();
                    }
                    _httpClient = (HttpClient)HttpClients.custom().setSslcontext(sslContext).setSSLHostnameVerifier((HostnameVerifier)new NoopHostnameVerifier()).setDefaultRequestConfig(requestConfig).build();
                }
            }
        return _httpClient;
    }
}
