

## link
https://hc.apache.org/httpclient-legacy/index.html
https://github.com/apache/httpcomponents-client/blob/master/httpclient5/src/test/java/org/apache/hc/client5/http/examples/ClientConnectionConfig.java


## HttpClient的两种重试机制
```java
//https://segmentfault.com/a/1190000037713035 服务不可用重试
DefaultServiceUnavailableRetryStrategy serviceUnavailableRetryStrategy = new DefaultServiceUnavailableRetryStrategy();
httpClient = HttpClients.custom().setServiceUnavailableRetryStrategy(serviceUnavailableRetryStrategy).build();

@Override
public boolean retryRequest(final HttpResponse response, final int executionCount, final HttpContext context) {
    return executionCount <= maxRetries &&
        response.getStatusLine().getStatusCode() == HttpStatus.SC_SERVICE_UNAVAILABLE;
}
```

## ssl HttpClients.custom
```java
   SSLContext sslContext = null;
            try {
                sslContext = SSLContexts.custom().loadTrustMaterial(null, (TrustStrategy)new TrustStrategy() {
                    public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                        return true;
                    }
                }).build();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            }
            CloseableHttpClient httpClient = HttpClients.custom().setSslcontext(sslContext).setSSLHostnameVerifier((HostnameVerifier)new NoopHostnameVerifier()).setDefaultRequestConfig(requestConfig).build();
            return (HttpClient)httpClient;
```


##  ssl CloseableHttpAsyncClient
```java
// https://www.jianshu.com/p/721e34e44efb  org.apache.http.conn.ssl.SSLConnectionSocketFactory;
  TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
            public boolean isTrusted(X509Certificate[] certificate, String authType) {
                return true;
            }
        };
        SSLContext sslContext = SSLContexts.custom()
                .loadTrustMaterial(null, acceptingTrustStrategy).build();

        CloseableHttpAsyncClient client = HttpAsyncClients.custom()
                .setSSLHostnameVerifier(SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)
                .setSSLContext(sslContext).build();
        client.start();

        HttpGet request = new HttpGet("https://www.baidu.com");
        Future<HttpResponse> future = client.execute(request, null);
        HttpResponse response = future.get();
        String result = EntityUtils.toString(response.getEntity());
```


## frombody
```dotnetcli
   @Test
    public void testConstructorCompat() throws Exception {
        final File tmp= File.createTempFile("test", "test");
        tmp.deleteOnExit();
        final FileBody obj=new FileBody(tmp, "application/octet-stream");
        Assert.assertEquals(tmp.getName(), obj.getFilename());
    }
```
## InputStreamBody
```java
    final byte[] stuff = "Stuff".getBytes(Consts.ASCII);
        final InputStreamBody b1 = new InputStreamBody(new ByteArrayInputStream(stuff), "stuff");
```

# EntityBuilder.create json
```java
//https://github.com/apache/httpcomponents-client/blob/master/httpclient5/src/test/java/org/apache/hc/client5/http/examples/ClientMultipartFormPost.java
@Test
    public void testExplicitContentProperties() throws Exception {
        final HttpEntity entity = EntityBuilder.create()
            .setContentType(ContentType.APPLICATION_JSON)
            .setContentEncoding("identity")
            .setBinary(new byte[] {0, 1, 2})
            .setText("{\"stuff\"}").build();
        Assert.assertNotNull(entity);
        Assert.assertNotNull(entity.getContentType());
        Assert.assertEquals("application/json; charset=UTF-8", entity.getContentType().getValue());
        Assert.assertNotNull(entity.getContentEncoding());
        Assert.assertEquals("identity", entity.getContentEncoding().getValue());
        Assert.assertEquals("{\"stuff\"}", EntityUtils.toString(entity));
    }
```