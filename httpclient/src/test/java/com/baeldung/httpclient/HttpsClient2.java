/**
 * com.baeldung.httpclient - the name of the target package where the new class or interface will be created.
 * httpclient - the name of the current project.
 * null.java - the name of the PHP file that will be created.
 * HttpsClient2 - the name of the new file which you specify in the New File dialog box during the file creation.
 * Administrator wei.zhou@macrowing.com - the login name of the current user.
 * 2022/11/130:31
 **/
package com.baeldung.httpclient;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

/*
 * 测试不通过*/
public class HttpsClient2 {

    public CloseableHttpClient getCloseableHttpsClient() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        // SSLContextBuilder
        SSLContextBuilder builder = new SSLContextBuilder()
                .loadTrustMaterial(null, new TrustSelfSignedStrategy())
                .setSecureRandom(null)
                .useProtocol("TLSv1.3");
        // SSLContext
        SSLContext sslContext = builder.build();
        // SSLConnectionSocketFactory
        SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(
                sslContext, null, null, new NoopHostnameVerifier()
        );
        // Registry
        Registry<ConnectionSocketFactory> registry = RegistryBuilder.
                <ConnectionSocketFactory> create()
                .register("http", new PlainConnectionSocketFactory())
                .register("https", sslConnectionSocketFactory)
                .build();
        // ConnectionManager
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(registry);
        cm.setMaxTotal(2000);
        // client
        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(sslConnectionSocketFactory)
                .setConnectionManager(cm)
                .build();
        return httpClient;
    }

}

