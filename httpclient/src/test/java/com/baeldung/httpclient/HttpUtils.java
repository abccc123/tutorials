package com.baeldung.httpclient;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Vector;


public class HttpUtils {

    private String defaultContentEncoding;


    public static final int SMALL_TIME = 5000;
    public static final int BIG_TIME = 8000;
    private static final String HTTP = "http";
    private static final String HTTPS = "https";
    private static SSLConnectionSocketFactory sslsf = null;
    private static PoolingHttpClientConnectionManager cm = null;
    private static SSLContextBuilder builder = null;
    private static boolean isSkipHttps = true;

    static {
        if (isSkipHttps) {
            skipHttpclientInit();
            skinHttpURLConnectionInit();
        }
    }

    public HttpUtils() {
        this.defaultContentEncoding = Charset.defaultCharset().name();
    }

    // httpclient
    public static String httpContentTypePost(String url, HttpEntity httpEntity, String contenType) {
        HttpClient httpclient = getHttpClient();
        HttpPost httppost = getHttpPost(url);
        httppost.addHeader("Content-Type", contenType);
        httppost.setEntity(httpEntity);
        try {
            HttpResponse res = httpclient.execute(httppost);
            if (res.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                HttpEntity entity = res.getEntity();
                String result = EntityUtils.toString(entity);
                return result;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return null;

    }

    /**
     * http post请求
     *
     * @param url
     * @param httpEntity
     * @param cookies
     * @return
     */
    public static String httpPost(String url, HttpEntity httpEntity, String cookies) {
        HttpClient httpclient = getHttpClient();
        HttpPost httppost = getHttpPost(url);
        if (StringUtils.isNotBlank(cookies)) {
            httppost.setHeader("Cookies", cookies);
        }
        httppost.addHeader("Content-Type", "multipart/form-data; boundary=----WebKitFormBoundarymx2fSWqWSd0OxQqq");
        httppost.setEntity(httpEntity);
        try {
            HttpResponse res = httpclient.execute(httppost);
            if (res.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                HttpEntity entity = res.getEntity();
                String result = EntityUtils.toString(entity);
                return result;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return null;

    }

    /**
     * post请求
     *
     * @param url
     * @param jsonObject object
     * @return json object
     */
    public static String doPostJsonParam(String url, com.alibaba.fastjson2.JSONObject jsonObject) {
        HttpClient client = getHttpClient();
        HttpPost post = getHttpPost(url);
        try {
            StringEntity s = new StringEntity(jsonObject.toString(), "utf-8");
            s.setContentEncoding("utf-8");
            s.setContentType("application/json");
            post.setEntity(s);
            HttpResponse res = client.execute(post);
            if (res.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                HttpEntity entity = res.getEntity();
                String result = EntityUtils.toString(entity);
                return result;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return null;
    }
    // end httpclient

    // HttpURLConnection


    /**
     * 发送HttpPost请求
     *
     * @param strURL 服务地址
     * @param params json字符串,例如: "{ \"id\":\"12345\" }" ;其中属性名必须带双引号<br/>
     * @return 成功:返回json字符串<br/>
     */
    public static String post(String strURL, String params) throws Exception {
        System.out.println(strURL);
        System.out.println(params);
        BufferedReader reader = null;
        try {
            URL url = new URL(strURL);// 创建连接
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);
            connection.setRequestMethod("POST"); // 设置请求方式
            // connection.setRequestProperty("Accept", "application/json"); // 设置接收数据的格式
            connection.setRequestProperty("Content-Type", "application/json"); // 设置发送数据的格式
            connection.connect();
            //一定要用BufferedReader 来接收响应， 使用字节来接收响应的方法是接收不到内容的
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), StandardCharsets.UTF_8); // utf-8编码
            out.append(params);
            out.flush();
            out.close();
            // 读取响应
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
            String line;
            String res = "";
            while ((line = reader.readLine()) != null) {
                res += line;
            }
            reader.close();

            //如果一定要使用如下方式接收响应数据， 则响应必须为: response.getWriter().print(StringUtils.join("{\"errCode\":\"1\",\"errMsg\":\"", message, "\"}")); 来返回
//            int length = (int) connection.getContentLength();// 获取长度
//            if (length != -1) {
//                byte[] data = new byte[length];
//                byte[] temp = new byte[512];
//                int readLen = 0;
//                int destPos = 0;
//                while ((readLen = is.read(temp)) > 0) {
//                    System.arraycopy(temp, 0, data, destPos, readLen);
//                    destPos += readLen;
//                }
//                String result = new String(data, "UTF-8"); // utf-8编码
//                System.out.println(result);
//                return result;
//            }

            return res;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * POST请求数据
     *
     * @param url
     * @param params
     * @return
     * @throws Exception
     */
    public static String doPost(String url, Map<String, String> params) throws Exception {
        HttpURLConnection connection = (HttpURLConnection) sendPostRequest(url, params, null);
        int code = connection.getResponseCode();
        if (code == 200) {
            InputStream inputStream = connection.getInputStream();
            if (inputStream != null) {
                return StringHelper.InputStreamTOString(inputStream);
            }
        }
        return null;
    }

    /**
     * POST请求数据
     *
     * @param url
     * @param params
     * @param restfulParams
     * @return
     * @throws Exception
     */
    public static String doPostObjectParam(String url, Map<String, Object> params, String restfulParams) throws Exception {
        HttpURLConnection connection = (HttpURLConnection) sendPostRequestObjectParam(url, params, null, restfulParams);
        int code = connection.getResponseCode();
        if (code == 200) {
            InputStream inputStream = connection.getInputStream();
            if (inputStream != null) {
                return StringHelper.InputStreamTOString(inputStream);
            }
        }
        return null;
    }

    /**
     * 通过GET方式获取数据
     *
     * @param httpUrl
     * @param params
     * @return
     * @throws Exception
     */
    public static String doGet(String httpUrl, Map<String, Object> params, String restfulParams) throws Exception {
        Map<String, Object> header = new HashMap<String, Object>();
//        header.put("contentType", "text/html");
        HttpURLConnection con = (HttpURLConnection) sendGetRequest(httpUrl, params, header, restfulParams);
        int code = con.getResponseCode();
        if (code == 200) {
            InputStream inputStream = con.getInputStream();
            if (inputStream != null) {
                return StringHelper.InputStreamTOString(inputStream);
            }
        }
        return null;
    }

    /**
     * 通过GET方式获取数据
     *
     * @param httpUrl
     * @param params
     * @return
     * @throws Exception
     */
    public static String doGetContentTypeJson(String httpUrl, Map<String, Object> params, String restfulParams) throws Exception {
        Map<String, Object> header = new HashMap<String, Object>();
        header.put("contentType", "text/html");
        HttpURLConnection con = (HttpURLConnection) sendGetRequest(httpUrl, params, header, restfulParams);
        int code = con.getResponseCode();
        if (code == 200) {
            InputStream inputStream = con.getInputStream();
            if (inputStream != null) {
                return StringHelper.InputStreamTOString(inputStream);
            }
        }
        return null;
    }

    public static String sendHttpGet(String url, Map<String, Object> params, String restfulParams) throws Exception {
        String result = doGet(url, params, restfulParams);
        return result;
    }
    // end HttpURLConnection


    /**
     * 发送GET请求
     *
     * @param url
     * @param params
     * @param headers
     * @return
     * @throws Exception
     */
    private static URLConnection sendGetRequest(String url, Map<String, Object> params, Map<String, Object> headers, String restfulParams)
            throws Exception {
        url = setRestfulParams(url, params, restfulParams);
        StringBuilder buf = new StringBuilder(url);
        Set<Map.Entry<String, Object>> entrys = null;
        // 如果是GET请求，则请求参数在URL中
        if (params != null && !params.isEmpty()) {
            buf.append("?");
            entrys = params.entrySet();
            for (Map.Entry<String, Object> entry : entrys) {
                String key = entry.getValue() == null ? "" : entry.getValue().toString();
                buf.append(entry.getKey()).append("=")
                        .append(URLEncoder.encode(key, "UTF-8"))
                        .append("&");
            }
            buf.deleteCharAt(buf.length() - 1);
        }
        URL url1 = new URL(buf.toString());
        HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
        conn.setRequestMethod("GET");
        // 设置请求头
        if (headers != null && !headers.isEmpty()) {
            entrys = headers.entrySet();
            for (Map.Entry<String, Object> entry : entrys) {
                conn.setRequestProperty(entry.getKey(), entry.getValue().toString());
            }
        }
        conn.getResponseCode();
        return conn;
    }

    /**
     * 发送POST请求
     *
     * @param url
     * @param params
     * @param headers
     * @return
     * @throws Exception
     */
    private static URLConnection sendPostRequest(String url,
                                                 Map<String, String> params, Map<String, String> headers)
            throws Exception {
        StringBuilder buf = new StringBuilder();
        Set<Map.Entry<String, String>> entrys = null;
        // 如果存在参数，则放在HTTP请求体，形如name=aaa&age=10
        if (params != null && !params.isEmpty()) {
            entrys = params.entrySet();
            for (Map.Entry<String, String> entry : entrys) {
                if (entry.getValue() == null) {
                    entry.setValue("");
                }
                buf.append(entry.getKey()).append("=").append(URLEncoder.encode(entry.getValue(), "UTF-8")).append("&");
            }
            buf.deleteCharAt(buf.length() - 1);
        }
        URL url1 = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        OutputStream out = conn.getOutputStream();
        out.write(buf.toString().getBytes(StandardCharsets.UTF_8));
        if (headers != null && !headers.isEmpty()) {
            entrys = headers.entrySet();
            for (Map.Entry<String, String> entry : entrys) {
                conn.setRequestProperty(entry.getKey(), entry.getValue());
            }
        }
        conn.getResponseCode(); // 为了发送成功
        return conn;
    }

    /**
     * 发送POST请求
     *
     * @param url
     * @param params
     * @param headers
     * @param restfulParams
     * @return
     * @throws Exception
     */
    private static URLConnection sendPostRequestObjectParam(String url, Map<String, Object> params,
                                                            Map<String, String> headers, String restfulParams)
            throws Exception {
        url = setRestfulParams(url, params, restfulParams);
        StringBuilder buf = new StringBuilder();
        Set<Map.Entry<String, Object>> entrys = null;
        // 如果存在参数，则放在HTTP请求体，形如name=aaa&age=10
        if (params != null && !params.isEmpty()) {
            entrys = params.entrySet();
            for (Map.Entry<String, Object> entry : entrys) {
                String value = entry.getValue() == null ? "" : entry.getValue().toString();
                buf.append(entry.getKey()).append("=")
                        .append(URLEncoder.encode(value, "UTF-8"))
                        .append("&");
            }
            buf.deleteCharAt(buf.length() - 1);
        }
        URL url1 = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        OutputStream out = conn.getOutputStream();
        out.write(buf.toString().getBytes(StandardCharsets.UTF_8));
        if (headers != null && !headers.isEmpty()) {
            Set<Map.Entry<String, String>> entrysHead = headers.entrySet();
            for (Map.Entry<String, String> entry : entrysHead) {
                conn.setRequestProperty(entry.getKey(), entry.getValue());
            }
        }
        conn.getResponseCode(); // 为了发送成功
        return conn;
    }

    /**
     * 设置restful参数
     *
     * @param url
     * @param params
     * @param restfulParams
     * @return
     */
    private static String setRestfulParams(String url, Map<String, Object> params, String restfulParams) {
        if (StringUtils.isNotBlank(restfulParams)) {
            String[] restfulParamArr = restfulParams.split(",");
            if (restfulParamArr != null && restfulParamArr.length > 0) {
                for (String key : restfulParamArr) {
                    Object value = params.get(key);
                    if (value != null) {
                        url = url.replace("{" + key + "}", value.toString());
                        params.remove(key);
                    }
                }
            }
        }
        return url;
    }

    /**
     * 将输入流转为字节数组
     *
     * @param inStream
     * @return
     * @throws Exception
     */
    private static byte[] read2Byte(InputStream inStream) throws Exception {
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outSteam.write(buffer, 0, len);
        }
        outSteam.close();
        inStream.close();
        return outSteam.toByteArray();
    }

    /**
     * 将输入流转为字符串
     *
     * @param inStream
     * @return
     * @throws Exception
     */
    private static String read2String(InputStream inStream) throws Exception {
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outSteam.write(buffer, 0, len);
        }
        outSteam.close();
        inStream.close();
        return new String(outSteam.toByteArray(), StandardCharsets.UTF_8);
    }

    /**
     * 发送xml数据
     *
     * @param path     请求地址
     * @param xml      xml数据
     * @param encoding 编码
     * @return
     * @throws Exception
     */
    private static byte[] postXml(String path, String xml, String encoding) throws Exception {
        byte[] data = xml.getBytes(encoding);
        URL url = new URL(path);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-Type", "text/xml; charset=" + encoding);
        conn.setRequestProperty("Content-Length", String.valueOf(data.length));
        conn.setConnectTimeout(5 * 1000);
        OutputStream outStream = conn.getOutputStream();
        outStream.write(data);
        outStream.flush();
        outStream.close();
        if (conn.getResponseCode() == 200) {
            return read2Byte(conn.getInputStream());
        }
        return null;
    }

    /**
     * 发送GET请求
     *
     * @param url
     * @param params
     * @param headers
     * @return
     * @throws Exception
     */
    private static URLConnection sendGetRequest(String url, Map<String, Object> params, Map<String, Object> headers)
            throws Exception {
        StringBuilder buf = new StringBuilder(url);
        Set<Map.Entry<String, Object>> entrys = null;
        // 如果是GET请求，则请求参数在URL中
        if (params != null && !params.isEmpty()) {
            buf.append("?");
            entrys = params.entrySet();
            for (Map.Entry<String, Object> entry : entrys) {
                String key = entry.getValue() == null ? "" : entry.getValue().toString();
                buf.append(entry.getKey()).append("=")
                        .append(URLEncoder.encode(key, "UTF-8"))
                        .append("&");
            }
            buf.deleteCharAt(buf.length() - 1);
        }
        URL url1 = new URL(buf.toString());
        HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
        conn.setRequestMethod("GET");
        // 设置请求头
        if (headers != null && !headers.isEmpty()) {
            entrys = headers.entrySet();
            for (Map.Entry<String, Object> entry : entrys) {
                conn.setRequestProperty(entry.getKey(), entry.getValue().toString());
            }
        }
        conn.getResponseCode();
        return conn;
    }


    /**
     * 发送GET请求
     *
     * @param urlString URL地址
     * @return 响应对象
     * @throws IOException
     */
    private HttpRespons sendGet(String urlString) throws IOException {
        return this.send(urlString, "GET", null, null);
    }

    /**
     * 发送GET请求
     *
     * @param urlString URL地址
     * @param params    参数集合
     * @return 响应对象
     * @throws IOException
     */
    private HttpRespons sendGet(String urlString, Map<String, String> params) throws IOException {
        return this.send(urlString, "GET", params, null);
    }

    /**
     * 发送GET请求
     *
     * @param urlString URL地址
     * @param params    参数集合
     * @param propertys 请求属性
     * @return 响应对象
     * @throws IOException
     */
    private HttpRespons sendGet(String urlString, Map<String, String> params, Map<String, String> propertys)
            throws IOException {
        return this.send(urlString, "GET", params, propertys);
    }

    /**
     * 发送POST请求
     *
     * @param urlString URL地址
     * @return 响应对象
     * @throws IOException
     */
    private HttpRespons sendPost(String urlString) throws IOException {
        return this.send(urlString, "POST", null, null);
    }

    /**
     * 发送POST请求
     *
     * @param urlString URL地址
     * @param params    参数集合
     * @return 响应对象
     * @throws IOException
     */
    private HttpRespons sendPost(String urlString, Map<String, String> params) throws IOException {
        return this.send(urlString, "POST", params, null);
    }

    /**
     * 发送HTTP请求
     *
     * @param urlString  地址
     * @param method     get/post
     * @param parameters 添加由键值对指定的请求参数
     * @param propertys  添加由键值对指定的一般请求属性
     * @return 响映对象
     * @throws IOException
     */
    private HttpRespons send(String urlString, String method, Map<String, String> parameters,
                             Map<String, String> propertys) throws IOException {
        HttpURLConnection urlConnection = null;

        if (method.equalsIgnoreCase("GET") && parameters != null) {
            StringBuffer param = new StringBuffer();
            int i = 0;
            for (String key : parameters.keySet()) {
                if (i == 0) {
                    param.append("?");
                } else {
                    param.append("&");
                }
                param.append(key).append("=").append(parameters.get(key));
                i++;
            }
            urlString += param;
        }

        URL url = new URL(urlString);
        urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod(method);
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);
        urlConnection.setUseCaches(false);

        if (propertys != null) {
            for (String key : propertys.keySet()) {
                urlConnection.addRequestProperty(key, propertys.get(key));
            }
        }

        if (method.equalsIgnoreCase("POST") && parameters != null) {
            StringBuffer param = new StringBuffer();
            for (String key : parameters.keySet()) {
                param.append("&");
                param.append(key).append("=").append(parameters.get(key));
            }
            urlConnection.getOutputStream().write(param.toString().getBytes());
            urlConnection.getOutputStream().flush();
            urlConnection.getOutputStream().close();
        }
        return this.makeContent(urlString, urlConnection);
    }

    /**
     * 得到响应对象
     *
     * @param urlConnection
     * @return 响应对象
     * @throws IOException
     */
    private HttpRespons makeContent(String urlString, HttpURLConnection urlConnection) throws IOException {
        HttpRespons httpResponser = new HttpRespons();
        try {
            InputStream in = urlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            httpResponser.contentCollection = new Vector<String>();
            StringBuffer temp = new StringBuffer();
            String line = bufferedReader.readLine();
            while (line != null) {
                httpResponser.contentCollection.add(line);
                temp.append(line).append("\r\n");
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
            String ecod = urlConnection.getContentEncoding();
            if (ecod == null) {
                ecod = this.defaultContentEncoding;
            }
            httpResponser.urlString = urlString;
            httpResponser.defaultPort = urlConnection.getURL().getDefaultPort();
            httpResponser.file = urlConnection.getURL().getFile();
            httpResponser.host = urlConnection.getURL().getHost();
            httpResponser.path = urlConnection.getURL().getPath();
            httpResponser.port = urlConnection.getURL().getPort();
            httpResponser.protocol = urlConnection.getURL().getProtocol();
            httpResponser.query = urlConnection.getURL().getQuery();
            httpResponser.ref = urlConnection.getURL().getRef();
            httpResponser.userInfo = urlConnection.getURL().getUserInfo();
            httpResponser.content = new String(temp.toString().getBytes(), ecod);
            httpResponser.contentEncoding = ecod;
            httpResponser.code = urlConnection.getResponseCode();
            httpResponser.message = urlConnection.getResponseMessage();
            httpResponser.contentType = urlConnection.getContentType();
            httpResponser.method = urlConnection.getRequestMethod();
            httpResponser.connectTimeout = urlConnection.getConnectTimeout();
            httpResponser.readTimeout = urlConnection.getReadTimeout();
            return httpResponser;
        } catch (IOException e) {
            throw e;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }

    /**
     * 发送POST请求
     *
     * @param urlString URL地址
     * @param params    参数集合
     * @param propertys 请求属性
     * @return 响应对象
     * @throws IOException
     */
    private HttpRespons sendPost(String urlString, Map<String, String> params, Map<String, String> propertys)
            throws IOException {
        return this.send(urlString, "POST", params, propertys);
    }

    /**
     * 默认的响应字符集
     */
    private String getDefaultContentEncoding() {
        return this.defaultContentEncoding;
    }

    /**
     * 设置默认的响应字符集
     */
    private void setDefaultContentEncoding(String defaultContentEncoding) {
        this.defaultContentEncoding = defaultContentEncoding;
    }

    // httpclient

    private static void skipHttpclientInit() {
        try {
            builder = new SSLContextBuilder();
            // 全部信任,不做身份鉴定.
            builder.loadTrustMaterial(null, new TrustStrategy() {
                public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                    return true;
                }
            });
            sslsf = new SSLConnectionSocketFactory(builder.build(), new String[]{"SSLv2Hello", "SSLv3", "TLSv1", "TLSv1.2"}, null, NoopHostnameVerifier.INSTANCE);
            Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create().register(HTTP, new PlainConnectionSocketFactory()).register(HTTPS, sslsf).build();
            cm = new PoolingHttpClientConnectionManager(registry);
            cm.setMaxTotal(200);// max connection
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取httpclient
     *
     * @return
     */
    private static HttpClient getHttpClient() {
        if (isSkipHttps) {
            SSLContext sslContext = null;
            try {
                sslContext = SSLContexts.custom().loadTrustMaterial(null, (TrustStrategy)new TrustStrategy() {
                    public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                        return true;
                    }
                }).build();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            }
            return (HttpClient)HttpClients.custom().setSslcontext(sslContext)
                    .setSSLHostnameVerifier((HostnameVerifier)new NoopHostnameVerifier()).build();
        }
        return (HttpClient)new DefaultHttpClient();

//        if (isSkipHttps) {
//            return HttpClients.custom().setSSLSocketFactory(sslsf).setConnectionManager(cm).setConnectionManagerShared(true).build();
//        }
//        return new DefaultHttpClient();
    }

    /**
     * 设置shangchuang
     *
     * @param url
     * @return
     */
    private static HttpPost getHttpPost(String url) {
        if (isSkipHttps) {
            HttpPost post = new HttpPost(url);
            RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(SMALL_TIME).setConnectTimeout(SMALL_TIME).setSocketTimeout(SMALL_TIME).build();
            post.setConfig(requestConfig);
            return post;
        }
        return new HttpPost(url);

    }
    // end httpclient

    //    HttpURLConnection 跳过https
    private static void skinHttpURLConnectionInit() {
        try {
            trustAllHttpsCertificates();
            HttpsURLConnection.setDefaultHostnameVerifier
                    (
                            new HostnameVerifier() {
                                @Override
                                public boolean verify(String urlHostName, SSLSession session) {
                                    // TODO Auto-generated method stub
                                    return true;
                                }
                            }
                    );
        } catch (Exception e) {
        }
    }

    private static void trustAllHttpsCertificates() throws NoSuchAlgorithmException, KeyManagementException {
        TrustManager[] trustAllCerts = new TrustManager[1];
        trustAllCerts[0] = new TrustAllManager();
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(
                sc.getSocketFactory());
    }

    private static class TrustAllManager implements X509TrustManager {
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public void checkServerTrusted(X509Certificate[] certs,
                                       String authType)
                throws CertificateException {
        }

        public void checkClientTrusted(X509Certificate[] certs,
                                       String authType)
                throws CertificateException {
        }
    }

    //   end HttpURLConnection 跳过https
}
