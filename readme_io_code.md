## Java URLConnection NTLM代理身份验证 - linux

```java
//https://www.javaroad.cn/questions/286403
proxy = new Proxy( Proxy.Type.HTTP, new InetSocketAddress( host, 80 ) );
    Authenticator.setDefault(new Authenticator() {
        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            logger.info("Getting password authentication...");
            return new PasswordAuthentication(
                    "DOMAIN\\user",
                    "password".toCharArray() ) ;
        }
    });
    connection = (HttpURLConnection) url.openConnection( proxy );
    String credentials = username + ":" + password;
    String basicAuth = "Basic " + Base64.encode( credentials.getBytes() );
    connection.setRequestProperty( "Authorization", basicAuth );
    connection.setRequestMethod( "GET" );

    //username/password above != user/pass below
    String proxyAuth = Base64.encode( ("user:pass").getBytes() );
    proxyAuth = "Basic " + proxyAuth;
    connection.setRequestProperty( "Proxy-Connection", "Keep-Alive" );
    connection.setRequestProperty( "Proxy-Authorization", proxyAuth );

    connection.connect();
```

## file  InputStream 流

```java
//core-java-modules/core-java-io-2/src/test/java/com/baeldung/copyfiles/FileCopierIntegrationTest.java
File copied = new File("src/test/resources/copiedWithIo.txt");
        try (InputStream in = new BufferedInputStream(new FileInputStream(original)); OutputStream out = new BufferedOutputStream(new FileOutputStream(copied))) {
            byte[] buffer = new byte[1024];
            int lengthRead;
            while ((lengthRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, lengthRead);
                out.flush();
            }
        }
        assertThat(copied).exists();
        assertThat(Files.readAllLines(original.toPath()).equals(Files.readAllLines(copied.toPath())));
```

## File

if (!original.exists())  ／／是否存在
Files.createFile(original.toPath());／／创建

FileUtils.copyFile(original, copied);／／复制
private static final File TEMP_DIRECTORY = new File(System.getProperty("java.io.tmpdir"));　

## 目录操作 创建 删除 列表 写入

```java
//core-java-modules/core-java-io-2/src/test/java/com/baeldung/directories/NewDirectoryUnitTest.java
   File newDirectory = new File(TEMP_DIRECTORY, "new_directory");
           File nestedInNewDirectory = new File(newDirectory, "nested_directory");
           File existingDirectory = new File(TEMP_DIRECTORY, "existing_directory");
           File existingNestedDirectory = new File(existingDirectory, "existing_nested_directory");
           File nestedInExistingDirectory = new File(existingDirectory, "nested_directory");

           nestedInNewDirectory.delete();
           newDirectory.delete();
           nestedInExistingDirectory.delete();
           existingDirectory.mkdir();
           existingNestedDirectory.mkdir();

           existingDirectory.exists()//存在
           core-java-modules/core-java-io-2/src/test/java/com/baeldung/listfiles/ListFilesUnitTest.java //列表
           core-java-modules/core-java-io-2/src/test/java/com/baeldung/writetofile/JavaWriteToFileUnitTest.java //写入
```

## Java下载文件的几种方式

```java
//1.以流的方式下载.
public HttpServletResponse download(String path, HttpServletResponse response) {
        try {
            // path是指欲下载的文件的路径。
            File file = new File(path);
            // 取得文件名。
            String filename = file.getName();
            // 取得文件的后缀名。
            String ext = filename.substring(filename.lastIndexOf(".") + 1).toUpperCase();

            // 以流的形式下载文件。
            InputStream fis = new BufferedInputStream(new FileInputStream(path));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            // 设置response的Header
            response.addHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes()));
            response.addHeader("Content-Length", "" + file.length());
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return response;
    }

2.下载本地文件

public void downloadLocal(HttpServletResponse response) throws FileNotFoundException {
        // 下载本地文件
        String fileName = "Operator.doc".toString(); // 文件的默认保存名
        // 读到流中
        InputStream inStream = new FileInputStream("c:/Operator.doc");// 文件的存放路径
        // 设置输出的格式
        response.reset();
        response.setContentType("bin");
        response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        // 循环取出流中的数据
        byte[] b = new byte[100];
        int len;
        try {
            while ((len = inStream.read(b)) > 0)
                response.getOutputStream().write(b, 0, len);
            inStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

public void downloadNet(HttpServletResponse response) throws MalformedURLException {
        // 下载网络文件
        int bytesum = 0;
        int byteread = 0;

        URL url = new URL("windine.blogdriver.com/logo.gif");

        try {
            URLConnection conn = url.openConnection();
            InputStream inStream = conn.getInputStream();
            FileOutputStream fs = new FileOutputStream("c:/abc.gif");

            byte[] buffer = new byte[1204];
            int length;
            while ((byteread = inStream.read(buffer)) != -1) {
                bytesum += byteread;
                System.out.println(bytesum);
                fs.write(buffer, 0, byteread);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
```

## Java中File常用的方法汇总

创建：
createNewFile()在指定位置创建一个空文件，成功就返回true，如果已存在就不创建，然后返回false。
mkdir() 在指定位置创建一个单级文件夹。
mkdirs() 在指定位置创建一个多级文件夹。
renameTo(File dest)如果目标文件与源文件是在同一个路径下，那么renameTo的作用是重命名， 如果目标文件与源文件不是在同一个路径下，那么renameTo的作用就是剪切，而且还不能操作文件夹。

删除：
delete() 删除文件或者一个空文件夹，不能删除非空文件夹，马上删除文件，返回一个布尔值。
deleteOnExit()jvm退出时删除文件或者文件夹，用于删除临时文件，无返回值。
判断：
exists() 文件或文件夹是否存在。
isFile() 是否是一个文件，如果不存在，则始终为false。
isDirectory() 是否是一个目录，如果不存在，则始终为false。
isHidden() 是否是一个隐藏的文件或是否是隐藏的目录。
isAbsolute() 测试此抽象路径名是否为绝对路径名。
获取：
getName() 获取文件或文件夹的名称，不包含上级路径。
getAbsolutePath()获取文件的绝对路径，与文件是否存在没关系
length() 获取文件的大小（字节数），如果文件不存在则返回0L，如果是文件夹也返回0L。
getParent() 返回此抽象路径名父目录的路径名字符串；如果此路径名没有指定父目录，则返回null。
lastModified()获取最后一次被修改的时间。

文件夹相关：
static File[] listRoots()列出所有的根目录（Window中就是所有系统的盘符）
list() 返回目录下的文件或者目录名，包含隐藏文件。对于文件这样操作会返回null。
listFiles() 返回目录下的文件或者目录对象（File类实例），包含隐藏文件。对于文件这样操作会返回null。
list(FilenameFilter filter)返回指定当前目录中符合过滤条件的子文件或子目录。对于文件这样操作会返回null。
listFiles(FilenameFilter filter)返回指定当前目录中符合过滤条件的子文件或子目录。对于文件这样操作会返回null。

```java
package com.file;

import java.io.File;
import java.io.IOException;

/**
 * file相关的方法
 * 
 * @author coco_xu
 *
 */
public class FileMethodTest {

    public static void main(String[] args) throws IOException {

        File fileCreate = new File("D:\\coco_xu\\study\\files\\file.txt");
        // 创建单级文件夹
        System.out.println("单级文件夹创建：" + fileCreate.mkdir());
        // 创建多级文件夹
        System.out.println("多级文件夹创建：" + fileCreate.mkdirs());
//创建文件
        System.out.println("创建文件：" + fileCreate.createNewFile());
//文件重命名
        File toFile = new File("D:\\coco_xu\\study\\files\\toFile.txt");
        System.out.println("文件重命名：" + fileCreate.renameTo(toFile));

        // 删除方法

        File file = new File("D:\\coco_xu\\study\\files\\toFile.tx");
        System.out.println("删除文件：" + file.delete());
        file.deleteOnExit();

        // 判断方法
        /*
         * File file = new File("F:\\a.txt");
         * System.out.println("文件或者文件夹存在吗？"+file.exists());
         * System.out.println("是一个文件吗？"+file.isFile());
         * System.out.println("是一个文件夹吗？"+file.isDirectory());
         * System.out.println("是隐藏文件吗？"+file.isHidden());
         * System.out.println("此路径是绝对路径名？"+file.isAbsolute());
         */

        // 获取方法
        /*
         * File file = new File("f:\\a.txt");
         * System.out.println("文件或者文件夹得名称是："+file.getName());
         * System.out.println("绝对路径是："+file.getPath());
         * System.out.println("绝对路径是："+file.getAbsolutePath());
         * System.out.println("文件大小是（以字节为单位）:"+file.length());
         * System.out.println("父路径是"+file.getParent()); //使用日期类与日期格式化类进行获取规定的时间 long
         * lastmodified= file.lastModified(); Date data = new Date(lastmodified);
         * SimpleDateFormat simpledataformat = new
         * SimpleDateFormat("YY年MM月DD日 HH:mm:ss");
         * System.out.println("最后一次修改的时间是："+simpledataformat.format(data));
         */

        // 文件或者文件夹的方法
        /*
         * File[] file = File.listRoots(); System.out.println("所有的盘符是："); for (File item
         * : file) { System.out.println("\t" + item); } File filename = new
         * File("D:\\coco_xu"); String[] name = filename.list();
         * System.out.println("指定文件夹下的文件或者文件夹有："); for (String item : name) {
         * System.out.println("\t" + item); } File[] f = filename.listFiles();
         * System.out.println("获得该路径下的文件或文件夹是："); for (File item : f) {
         * System.out.println("\t" + item.getName()); }
         */
    }

}
```

## HttpURLConnection 用法

core-java-modules\URLConnectionUtil.java



# aliyun\oss

D:\codeD\aliyun-oss-java-sdk-master\src\main\java\com\aliyun\oss\common\comm\RetryStrategy.java

 int scale = DEFAULT_RETRY_PAUSE_SCALE;
        long delay = (long) Math.pow(2, retries) * scale;


# [Java IO](https://github.com/frank-lam/fullstack-tutorial/blob/master/notes/JavaArchitecture/04-Java-IO.md#java-io)

https://github.com/frank-lam/fullstack-tutorial/blob/master/notes/JavaArchitecture/04-Java-IO.md#1%E7%A3%81%E7%9B%98%E6%93%8D%E4%BD%9Cfile
