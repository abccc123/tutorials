package com.example.demo2;

import com.alibaba.fastjson.JSON;
import okhttp3.*;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

public class Test {
    public static void main(String[] args) throws IOException {

        //相关依赖
//        <dependency>
//            <groupId>com.squareup.okhttp3</groupId>
//            <artifactId>okhttp</artifactId>
//            <version>4.9.0</version>
//        </dependency>
//
//        <dependency>
//            <groupId>com.alibaba</groupId>
//            <artifactId>fastjson</artifactId>
//            <version>1.2.66</version>
//        </dependency>

        //type 对应地址 https://blog.csdn.net/rgbhi/article/details/120905963
        File file = new File("D:\\CentOS\\CentOS-7-x86_64-Everything-2009.iso");
        String name = file.getName();
        long size = file.length();
        int chunkSize = 5242880;
        int index = 0;
        String token = "00006af76370dad34f64a25296ac4be64eaf";

        OkHttpClient client = new OkHttpClient().newBuilder().build();
        MediaType mediaType = MediaType.parse("text/plain");
        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("token", token)
                .addFormDataPart("folderId", "145")
                .addFormDataPart("fileName", name)
                .addFormDataPart("fileRemark", "大文件上传测试")
                .addFormDataPart("size", size + "")
                .addFormDataPart("type", "application/x-cd-image")
                .addFormDataPart("attachType", "0")
                .addFormDataPart("fileModel", "UPLOAD")
                .build();
        Request request = new Request.Builder()
                .url("http://xxsoft.net:/WebCore?module=RegionDocOperationApi&fun=CheckAndCreateDocInfo")
                .method("POST", body)
                .build();
        Response response = client.newCall(request).execute();
        String s = response.body().byteString().utf8();
        String regionHash = JSON.parseObject(s).getJSONObject("data").getString("RegionHash");
        String regionId = JSON.parseObject(s).getJSONObject("data").getString("RegionId");
        BufferedInputStream bufferedInputStream = null;
        try {
            bufferedInputStream = new BufferedInputStream(new FileInputStream(file), chunkSize);
            // 定义个8kb字节数组,作为缓冲区流
            byte[] bytes = new byte[chunkSize];
            int len = 0;
            while ((len = bufferedInputStream.read(bytes)) != -1) {
                byte[] bytes1 = new byte[len];
                System.arraycopy(bytes,0,bytes1,0,len);
                mediaType = MediaType.parse("text/plain");
                body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("uploadId", UUID.randomUUID().toString())
                        .addFormDataPart("regionHash", regionHash)
                        .addFormDataPart("regionId", regionId)
                        .addFormDataPart("fileName", name)
                        .addFormDataPart("size", size + "")
                        .addFormDataPart("chunks", (int) size / 5242880l + "")
                        .addFormDataPart("chunk", index + "")
                        .addFormDataPart("chunkSize", chunkSize + "")
                        .addFormDataPart("blockSize", len + "")
                        .addFormDataPart("file", name, RequestBody.create(bytes1, MediaType.parse("application/octet-stream")))
                        .build();
                request = new Request.Builder()
                        .url("http://xxsoft.net:/document/upload?code= &token=" + token)
                        .method("POST", body)
                        .build();
                response = client.newCall(request).execute();
                String s1 = response.body().byteString().utf8();
                System.out.println("" + JSON.toJSONString(s1));
                index++;
            }
        } catch (IOException e) {

        } finally {
            try {
                bufferedInputStream.close();
            } catch (IOException e) {
                throw new RuntimeException("关闭异常");
            }
        }
    }
}
